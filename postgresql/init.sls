include:
  - .phppgadmin

postgresql:

  pkg.latest:
  - pkgs:
    - {{ salt['pillar.get']('package:postgresql', 'postgresql') }}

  service.running:
  - enable: True
  - watch:
    - pkg: postgresql

base:

  '*':
  - filesystem
  - salt
  - ssh.known_hosts

  'salt':
  - salt.master

  'postgresql':
  - postgresql

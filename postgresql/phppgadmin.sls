phppgadmin:

  pkg.latest:
  - pkgs:
    - {{ salt['pillar.get']('package:phppgadmin', 'phppgadmin') }}
    - {{ salt['pillar.get']('package:php-pgsql', 'php-pgsql') }}
    - {{ salt['pillar.get']('package:php-fpm', 'php-fpm') }}
    - {{ salt['pillar.get']('package:nginx', 'nginx') }}

  file.symlink:
  - name: /srv/http/www
  - target: /usr/share/webapps/phppgadmin

phppgadmin-config:

  file.managed:
  - name: /srv/http/www/conf/config.inc.php
  - source: salt://postgresql/config.inc.php

phppgadmin-php.ini:

  file.managed:
  - name: /etc/php/php.ini
  - source: salt://postgresql/php.ini

phppgadmin-nginx:

  file.managed:
  - name: /etc/nginx/nginx.conf
  - source: salt://postgresql/nginx.conf
  - require:
    - pkg: phppgadmin

  service.running:
  - name: nginx
  - enable: True
  - watch:
    - file: phppgadmin-nginx
    - file: phppgadmin-php.ini

phppgadmin-php-fpm:

  service.running:
  - name: php-fpm
  - enable: True
  - watch:
    - file: phppgadmin-php.ini
